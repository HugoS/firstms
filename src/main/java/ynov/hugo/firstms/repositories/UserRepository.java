package ynov.hugo.firstms.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ynov.hugo.firstms.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{

	public User findByUsername(String username);

	public Optional<User> findByUsernameAndPassword(String username, String password);



	

}