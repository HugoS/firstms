package ynov.hugo.firstms.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ynov.hugo.firstms.model.User;
import ynov.hugo.firstms.repositories.UserRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController 
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping(value="/user/{id}")
	public Optional<User> getUser(@PathVariable long id){
		return userRepository.findById(id);
	}
	
	@GetMapping(value = "/users")
	public List<User> getUsers() {
		/*StringBuilder res= new StringBuilder();
		Iterable<User> users = userRepository.findAll();
		Iterator<User> ite = users.iterator();
		while(ite.hasNext()) {
			User u = ite.next();
			res.append(u.getUsername());
		}
		return res.toString();*/
		List<User> users= new ArrayList<User>();
		userRepository.findAll().forEach(users::add);
		System.out.println("ok");
		return users;
		
	}
	
	@PostMapping(value = "/add")
	public void addUser(@RequestBody User u) {
		userRepository.save(u);
	}

	@PutMapping(value = "/update")
	public void updateUser(@RequestBody User u) {
		userRepository.save(u);
	}
	@DeleteMapping(value = "/delete")
	public void deleteUser(@RequestBody User u) {
		userRepository.delete(u);
	}
	
	@GetMapping(value="/toto/{id}")
	public String toto(@PathVariable int id) {
		String result;
		switch(id) {
		case 1:
			result = "un";
			break;
		case 2:
			result = "deux";
			break;
		default:
			result = "rien...";
			break;
		}
		
		return result;
	}
	
	@PostMapping(value="/form")
	public void titi(@RequestBody String param) {
		System.out.println(param);
	}
	@PostMapping(value="/login")
	public Optional <User> login(@RequestBody User u) {
		return userRepository.findByUsernameAndPassword(u.getUsername(),u.getPassword());
	}
}
